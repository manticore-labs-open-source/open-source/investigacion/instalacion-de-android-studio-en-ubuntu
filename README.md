# Instalación de Android Studio en Ubuntu

Android Studio es el entorno de desarrollo integrado (IDE) oficial para el desarrollo de aplicaciones para Android y se basa en IntelliJ IDEA. Además del potente editor de códigos y las herramientas para desarrolladores de IntelliJ, Android Studio ofrece aún más funciones que aumentan tu productividad durante la compilación de apps para Android, como las siguientes:

* Un sistema de compilación basado en Gradle flexible
* Un emulador rápido con varias funciones
* Un entorno unificado en el que puedes realizar desarrollos para todos los dispositivos Android
* Instant Run para aplicar cambios mientras tu app se ejecuta sin la necesidad de compilar un nuevo APK
* Integración de plantillas de código y GitHub para ayudarte a compilar funciones comunes de las apps e importar ejemplos de código
* Gran cantidad de herramientas y frameworks de prueba
* Herramientas Lint para detectar problemas de rendimiento, usabilidad, compatibilidad de versión, etc.
* Compatibilidad con C++ y NDK
* Soporte incorporado para Google Cloud Platform, lo que facilita la integración de Google Cloud Messaging y App Engine.

Para la instalación en un sistema operativo Ubuntu, inicialmente se debe descargar la versión para Ubuntu, desde la [página oficial de Android Studio](https://developer.android.com/studio).

* Se debe seleccionar la opciones de descarga.
![Opciones de Descarga](Imagenes/opciones-descarga.png)

* Se debe escoger la versión para Linux:
![Version para Linux](Imagenes/version-ubuntu.png )

Una vez que se haya descargado Android Studio, se procederá a instalar y configurar las variables de entorno.

## Paso N° 1: Verificar la instalación de Javac
* Verificar si se tiene instalado javac. Para verificar se utilizará el siguiente comenzado: 
```
javac -version
```
* El cual ayudara a verificar si se tiene instalado.
* Nota: si se tiene instalado verificar que sea la versión 8 
```
java version 1.8.0_101
```
* Si no se tiende dicha versión es recomendable desinstalar e instalarla, para su desinstalación se usa el comando: 
```
sudo apt-get autoremove openjdk--jre
```
## Paso N° 2: instalación de Java Versión 8.
* **Nota: Si se tiene instalado omitir este paso.**

* Para la instalación de java se usa el siguiente comando: 
```
sudo apt install openjdk-8-jdk
```
* Verificar la versión de java con el comando:
 ```
 java -version 
 ```
 * Si no lograste instalar puedes seguir el [Tutorial de instalación de Java](https://www.digitalocean.com/community/tutorials/how-to-install-java-with-apt-on-ubuntu-18-04) 

## Paso N° 3: Variable de entorno para Java.
* Identificar donde se instalado Java y copiar la dirección para ello se usa el comando: 
```
sudo update-alternatives --config java
```
* Luego editar el archivo **.bashrc**, al final del archivo añadir la siguientes líneas:
```
export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
export  PATH=$JAVA_HOME/bin:$PATH
```
## Paso N° 4: Descomprimir Android Studio
* **Nota: Ejecutar sin sudo** 
* Descomprimir el archivo, luego ingresar a la carpeta descomprimida e ingresar a **/bin** y ejecutar **./studio.sh** para poder instalar Android Studio, siempre son sudo

## Paso N° 5: Variable de entorno para Android Studio
* **Nota: Se debe ejecutar el archivo .studio para inicializar Android Studio**
```
./studio
```
![image](Imagenes/android-studio.png)

* Dirigirse a configure y seleccionar SDK Manager
![image](Imagenes/SDK.png)

* Verificar el path de donde se encuentra instalado el Android Studio y copiarla
![image](Imagenes/SDK-path.png)

* Editar el archivo **.bashrc**, al final del archivo añadir la siguientes líneas:
```
export ANDROID_HOME=<path anteriormente copiado>
export PATH=${PATH}:$ANDROID_HOME/tools:$ANDROID_HOME/platform-tools
```

## Paso N° 6: Instalar Gradle
* Instalar Gradle con el siguiente comando:
```
sudo apt-get install gradle
```

<a href="https://twitter.com/kevi_n_24" target="_blank"><img alt="Sígueme en Twitter" height="35" width="35" src="https://3.bp.blogspot.com/-E4jytrbmLbY/XCrI2Xd_hUI/AAAAAAAAIAo/qXt-bJg1UpMZmTjCJymxWEOGXWEQ2mv3ACLcBGAs/s1600/twitter.png" title="Sígueme en Twitter"/> @kevi_n_24 </a><br>